const newKey = "drinksPerDay";

const guitarPlayer = {
    name: "Joe Satriani",
    age: 50,
    drumsYetatKa: false,
    jazzAptitude: undefined,
    sundarBaiko: null,
    albums: ["A", "B", "C"],
    address: {
        city: "LA",
        country: "USA"
    },
    playGuitar: function () {
        console.log('tananana')
    },
    // adding a key dynamically
    [newKey]: 1
    // drinksPerDay: 1
};


const cellA1 = "name";
// extract methods and properties
// . (dot) operator => cha
guitarPlayer.name; // "Joe Satriani"
guitarPlayer.address; // { city: "LA", country: "USA" }
guitarPlayer.address.city; // LA
guitarPlayer.playGuitar();
guitarPlayer.cellA1; // undefined -> no key like cellA1 exists in guitarPlayer

// ["key"]
guitarPlayer["name"]; // "Joe Satriani"
guitarPlayer["address"]; // { city: "LA", country: "USA" }
guitarPlayer["address"]["city"]; // LA
guitarPlayer["playGuitar"]();
guitarPlayer["cellA1"]; // undefined -> no key like cellA1 exists in guitarPlayer
guitarPlayer[cellA1]; // "Joe Satriani"

// modify the values
guitarPlayer["name"] = "Joseph Satriani";
guitarPlayer.address.city = "NY";

// add a new key: value
guitarPlayer.dob = "26/12/1970";
guitarPlayer["shoeSize"] = 10;

const lengthOfName = guitarPlayer.name.length;

//  destructure an object
// objects are destructured via keys
// const name = guitarPlayer.name;
// const address = guitarPlayer.address;

// constant chi nava = keys chi nava
const { name, albums, age, surname } = guitarPlayer;
// surname is not a key in guitarPlayer hence is undefined


// jasa arrays cha cloning problem ahe
// same problem object la pan ahe
// REFERENCE IS COPIED
// if guitar player changes clone also changes
// and vice a versa
const joSatrianisClone = guitarPlayer;

// spread operator -> ...
const newGuitarPlayer = { ...guitarPlayer };