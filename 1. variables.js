// number -> identifier -> variable name
// = -> assignment operator -> RHS la LHS madhe store karta
// 5 -> value to store -> literal
// inappropriate code -> no intention
// NEVER EVER EVER DO THIS.
number = 5;

// var
// var -> keyword of js. -> special words of a language
// var -> tells js that the intention is to create a "variable"
// var -> should be used only the first time the variable is used
var primeNumber = 7;

// dont use var here, primeNumber has been defined.
primeNumber = 11;

// declaring a variable.
// when variable is not assigned a value, it is declared.
// js puts a default value of something called 'undefined'
var negativeNumber; 

// assignment
negativeNumber = -10;


// let
// var & let are almost similar in usage
// diff -> nantar baghu
// syntax is exactly same

let decimalNumber; // declare a variable called decimalNumber
decimalNumber = 44.44; // assigned a value to decimalNumber

let rationalNumber = 100; // declaration + assignment

// const
const accelerationDueToGravity = 9.81;
accelerationDueToGravity = 10; // wont work. error denar

const numberOfLegs; // i cannot declare a constant
numberOfLegs = 2; // NOPE.
// declaration + assignment is compulsory in constants


// naming convention -> camelCasing
// first letter of first word -> small
// first letter of consecutive words -> capital
// RULE -> true constants are always ALL CAPS
const PI = 3.14;

// rule of thumb
// priority/usage of var, let & const
// const -> high preference -> use over let & var
// let   -> use over var -> to create a value that you KNOW is going to change
// var   -> NEVER USE
// always start by creating constants -> change later if required.


// prakarchya values
// value -> program cha data
// prakara -> type
// data che prakar -> datatype
// values that can be stored in variable