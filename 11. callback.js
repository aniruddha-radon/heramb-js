const getDataFromSheetOne = () => {
    const data = ["v11", "v21", "v31"];

    return data;
}

const getDataFromSheetTwo = () => {
    const data = ["v12", "v22", "v32"];

    return data;
}

// CALLBACK HOLDS THE REFERENCE OF A FUNCTION
// CALLBACK IS NOT A KEYWORD -> YOU CAN CALL IT ANYTHING
// yenaraFn => is the callback
const getDataFromSheet = yenaraFn => {
    // () => ["v13", "v23", "v33"]
    const data = yenaraFn();

    return data;
}

const sheet1 = getDataFromSheet(getDataFromSheetOne);
const sheet2 = getDataFromSheet(getDataFromSheetTwo);
const dynamicSheet = getDataFromSheet(() => ["v13", "v23", "v33"])

console.log(sheet1, sheet2, dynamicSheet);

// skipping closures for now.