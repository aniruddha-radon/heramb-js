// 2 booleans become 1
// AND -> &&
// OR  -> ||
// NOT -> !

const isValueEmpty = true;
const isLengthFive = true;

// const shouldProcess = isValueEmpty === false && isLengthFive === true
const shouldProcess = !isValueEmpty && isLengthFive;

// AND
true  && true  // true
true  && false // false
false && true  // false
false && false // false

// OR
true  || true  // true
true  || false // true
false || true  // true
false || false // false

// NOT
!true  // FALSE
!false // TRUE


// FALSY values -> values that are considered false in logical ops
// 0, "", NaN, null, undefined, false

// OR
// -> value which truthy is returned
// -> 2nd falsy values is returned
0 || "abcd"; // 0 is considered false => "abcd"
0 || ""; // => ""

// AND
// -> value which falsy is returned
// -> 2nd truthy values is returned
0 && "abcd"; // 0 is considered false => 0
0 && ""; // => 0
"abcd" && "pqrs"; // => pqrs



"pqrs" || 0 && "abcd";
    "pqrs" && "abcd"
        "pqrs" // -> check why this is the case
