
// register kela (installed in script)
// does not execute directly

// non parameterised
// non returning
function greet(/** parameters  */) {
    console.log('HELLLOOO');
}

// no () after fn name
const shortcut = greet; // REFERENCE OF THE FUNCTION

greet(); // CALL/EXECUTE THE FN
shortcut(); // CALLS THE GREET FN

// parameterised
// non returning

// name is parameter -> fn chya scope madhe
// it is treated like a "variable" not const
// parameters do not require let/var/const
function greetPerson(name) {
    // hello, <name>
    const message = `Hello, ${name}`;
    console.log(message);
}

greetPerson("Heramb"); // undefined

// non parameterised
// returning
function getGreeting() {
    const greeting = "HELLLLLOOOOOOOOO";

    // ek fn ek return statement execution
    // return STOPS the fn execution
    return greeting;

    // valid js code
    return 10;

    // also valid
    // returns undefined
    return;
}

const greeting = getGreeting(); // "HELLLLLOOOOOOOOO"


// parameterised
// returning
function checkIfDivisible(num1, num2) {
    if(num1 % num2 === 0) return num1;

    return num2;
}

// fn expression
// function () {

// }

// anonymous fn stored in a const
// add -> reference of the anonymous fn
const add = function (n1, n2) {
    return n1 + n2;
}

add(5, 7); // 12

// fat arrow functions => PREFERRED WAY
const subtract = (n1, n2) => {
    return n1 - n2;
}

// if only one parameter -> dont include () in parameters
const multiplyBy5 = n1 => {
    return n1 * 5;
}

multiplyBy5(10); // 50

// if only one executable line -> dont include {} 
// dont write return statement
// n1 is parameter
// returns n1 / 5
const divideBy5 = n1 => n1 / 5;

divideBy5(10); // 2

// const isPortPOL = PORT => PORT === POL


function naav() {
    // code
}

// function is not followed by name of fn
// function is followed by ()
const dusraNaav = function() {
    // code
}

// no function keyword
// no naav
// directly put () 
// => fat arrow 
// followed by {}
const tisraNaav = () => {
    // code
}

// 1. if fat arrow fn has only 1 parameter
// -> no need to wrap params by ()
// parameter is n
const multiplyBy10 = n => {
    return n * 10;
}

// 2. if fat arrow fn has only 1 statement/one line of code
// -> no need to write the scope of fn -> omit {}
// -> no need to write 'return' explicitly
// n is parameter
// n / 10 is code of function
// result will be returned automatically
const divideBy10 = n => n / 10;

divideBy10(50); // 5