// number
const integer = 10;
const decimal = 10.5;
const negative = -10;
const laskej = 10;

// typeof -> it gives back the type of data in the variable
// typeof always works on the value inside the const/var/let
// not the const/var/let itself
typeof laskej; // gives back 'number'

// string
// multiple ways to create strings 
// multiple quotes can be used -> '', "", ``
// string can contain ANYTHING inside quotes
// a-z, A-Z, 0-9, special chars
const singleQuotedString = '0a  &';
const doubleQuotedString = "9m% ";
const backTickedString = `hello Heramb!`;

// boolean
// always answers a question in yes or no | true or false
const isRaining = false;
const hadDinner = true;

// to log to the console
console.log(typeof isRaining);
console.log(typeof singleQuotedString);
console.log(typeof integer);

// undefined
const itsComplicated = undefined; // NEVER DO THIS
let brains;
// something declared but never assigned a value

// null
let girlFriend = "past gf";
girlFriend = null;
// null specifies no value

// array
// elements [values -> of any datatype], separated by commas
// are enclosed in []
const girlFriends = ["abcd", "pqrs", "lalala"];
const information = [
    "Heramb", 
    31, 
    false, 
    undefined, 
    null, 
    ["Aniruddha", "Om", "Pakya"],
    { city: "Berlin", country: "GM" },
    function () {
        console.log('blah blah blah');
    }
]; // usually an useless array

// object
// key (always have to be strings) -> name
// :
// value (can be of any datatype) -> "Heramb" 
// key: value pairs separated by commas and enclosed in {}
const person = {
    name: "Heramb",
    age: 31,
    isMarried: false,
    kids: undefined,
    guitar: null,
    friends: ["Aniruddha", "Om", "Pakya"],
    address: {
        city: "Berlin",
        country: "GM"
    },
    // functions which are inside an object
    // are called METHODS
    speak: function () {
        console.log('blah blah blah');
    }
}

// function
// fns need to be 'called' to be able execute code inside it
function greet() {
    // fn cha code
    console.log('hellooooo');
}

greet // not calling
greet() // calling a function -> trigger execution of the fn