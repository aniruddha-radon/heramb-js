// arithmetic operations

const num1 = 10;
const num2 = 2;
// addition +
const addition = num1 + num2;

// subtraction -
const difference = num1 - num2;

// multiplication *
const product = num1 * num2;

// division /
const quotient = num1 / num2;

// modulus % -> remainder
// if remainder is 0 -> num1 is completely divisible by num2
const remainder = num1 % num2;

// raised to - power - **
const power = num1 ** num2;

// ALL constants & variables are under the hood objects
// EVERYTHING IS AN OBJECT
// it has key value pairs -> value can be a method
// to access any property or method use the . operator on 
// the var/const/let
// properties and methods change according to the data type
// e.g
power.toFixed(2); // 100.00


// Math functions
// Math is class in JS -> which provides utility fns

const valueOfExcel = 78.987987213;
// this does not change original value of valueOfExcel
// converts into fixed decimal values
// automatically rounds it off
// gives result in string format not number format
const twoDecimals = valueOfExcel.toFixed(2);
console.log(twoDecimals);

// parseFloat
let numericValue = parseFloat(twoDecimals);
// parseInt -> takes only the integer part.
numericValue = parseInt(twoDecimals);

// in Math class round is a method
// method is a function defined inside a class
Math.round(twoDecimals); // 79
Math.ceil(twoDecimals);  // 79
Math.floor(twoDecimals); // 78

Math.random(); // random number between 0 to 1. 
// 0 & 1 is excluded

let sum = 10;
let v1 = 5;
let v2 = 10;

// sum = sum + v1 + v2;
sum += v1 + v2;
// sum = sum - v1
sum -= v1;
// sum = sum * (v1 + v2)
sum *= (v1 + v2);
// sum = sum / v1
sum /= v1;

// ajun ek goshta