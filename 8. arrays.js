// select range a1 to e1
const range = [
    ["A1", "B1", "C1", "D1", "E1"], // ROW 1 
    ["A2", "B2", "C2", "D2", "E2"],
    ["A3", "B3", "C3", "D3", "E3"],
    ["A4", "B4", "C4", "D4", "E4"],
    ["A5", "B5", "C5", "D5", "E5"],
];

// index ni access karne
range[0]; // ["A1", "B1", "C1", "D1", "E1"]
range[1]; // ["A2", "B2", "C2", "D2", "E2"]
range[4]; // ["A5", "B5", "C5", "D5", "E5"],
range[5]; // undefined
range[-1]; // undefined

// assign to index
// does not update sheet, only modifies the array.
range[0] = ["A1 modified", "B1 modified", "C1 modified", "D1 modified", "E1 modified"];
range[5] = ["A6"];

// row1 = ["A1", "B1", "C1", "D1", "E1"]
const row1 = range[0];
row1[0]; // "A1"

// extract element of array inside array
range[0][0]; // "A1"
range[0][0] = "navin value";
// ["navin value", "B1", "C1", "D1", "E1"]

// add element to array
const row = ["aniruddha", "heramb", "shreya"];

// add in the end
// push
row.push("om");
// ["aniruddha", "heramb", "shreya", "om"];

// add in front [0th index]
// unshift
row.unshift("pakya");
// ["pakya", "aniruddha", "heramb", "shreya", "om"];

// add anywhere 
// splice
// arg1 -> index of start point
// arg2 -> while adding, always 0, but means how many elements to delete
// arg3 and onward -> elements to add
row.splice(2, 0, "shantanu", "tantanu");
// ["pakya", "aniruddha", "shantanu", "tantanu", "heramb", "shreya", "om"];

// remove/delete

// remove last element
// pop
row.pop(); 
// ["pakya", "aniruddha", "shantanu", "tantanu", "heramb", "shreya"];

// remove first elmeent
// shift
row.shift();
// ["aniruddha", "shantanu", "tantanu", "heramb", "shreya"];

// remove from anywhere
// splice
row.splice(1, 2);
// ["aniruddha", "heramb", "shreya"];

// assume row has below value
// ["aniruddha", "heramb", "shreya", "heramb", "heramb"];
row.indexOf("aniruddha"); // 0
row.indexOf("shreya"); // 2
row.indexOf("heramb"); // 1
row.lastIndexOf("heramb"); // 
row.indexOf("om"); // -1

// callbacks shiklo ki parat yeun shiku
const bottles = ["wine", "whiskey", "water"];

const cellValueA1 = "water";
const cellValueA2 = "beer";

// findIndex take a callback fn. 
// the callback fn must return a boolean
// callback fn will receive each elem of array as parameter
bottles.findIndex(bottle => bottle === cellValueA1) //  2
bottles.findIndex(bottle => bottle === cellValueA2) // -1

// map -> transforms each element in a specific way 
// and returns A NEW ARRAY
// expected output -> ["WINE", "WHISKEY", "WATER"];
// callback fn -> takes the elem as param, 
// defines how the elem should be modified
const capitalisedBottles = bottles.map(bottle => bottle.toUpperCase());
// capitalisedBottles -> ["WINE", "WHISKEY", "WATER"];

// filter -> RETURNS A NEW ARRAY OF FILTERED ITEMS
bottles.filter(bottle => bottle.length > 4);
// ["WHISKEY", "WATER"]

// reduce -> skip

// every -> if callback returns true for 
// ALL ELEMENTS ONLY THEN result will be true
bottles.every(bottle => bottle[0] === "w");
// bottles.every(bottle => bottle.startsWith("w"));
const rowValues = ["", "", "", ""];
rowValues.every(column => !column);

// some
rowValues.some(column => column);

// destructuring
//                 "A1"     "B1"       "C1", "D1"
const footwears = ["shoes", "chappal", "",  "sandal"];
// extract and store
// const shoes = footwears[0]
// const chappal = footwears[1];
// const sandal = footwears[3];
const [shoes, chappal,,sandal] = footwears;

const herambCheJobs = ["A", "B", "C"];
// REFERENCE IS COPIED
const aniruddhaCheJobs = herambCheJobs;
herambCheJobs.push("D");
// ALL REFERENCES WILL ALSO UPDATE AUTOMATICALLY

// CLONING
// 1. slice
const shreyaCheJobs = herambCheJobs.slice();
herambCheJobs.push("E");

// 2. spread operator -> ...
// copy all the elements of array in other array
const pakyaCheJobs = [...herambCheJobs];
herambCheJobs.push("F");