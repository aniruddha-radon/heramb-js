const valueA1 = "Heramb";
const valueB1 = "Joshi";

// string concatenation -> OLD WAY
let fullName = valueA1 + " " + valueB1;

// string concatenation -> NEW WAY = ALWAYS USE THIS
// ${variable_name} => ${} signify that value is dynamic
// template variables -> string interpolation
fullName = `${valueA1} ${valueB1}`;

// length -> property not a method/fn
const nameLength = valueA1.length;

// indexes    01234
const word = "chair";
// accessing single chars from a string
word[0]; // c
word[3]; // i
word[5]; // undefined
word[-1]; // doesnt work -> undefined

word[4] = "n"; // does not work
// strings cannot be modified by character
// string value cannot be changed
// strings are immutable

// methods
word.includes("hair"); // true
word.includes("har");  // false
word.includes("air");  // true

const shippingTerms = "FY/CO";
const terms = shippingTerms.split("/"); 
// terms -> ["FY", "CO"]
// terms[0] -> "FY" | terms[1] -> "CO"

const suppliers = "unifeeder, bifeeder, trifeeder";
const supplierNames = suppliers.split(", ");
// ["unifeeder", "bifeeder", "trifeeder"]

const valueD1 = "     CL Kelang        "; 
//              "CL Kelang";
// trim always works on leading and trailing
// adhichya aani palikadchya white spaces
// madhlya spaces la haat lagat nahi
const actualValue = valueD1.trim();
// actualValue => "CL Kelang"