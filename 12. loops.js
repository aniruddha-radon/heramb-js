// 1. iterating over strings
// 2. iterating over arrays
// 3. iterating over objects

// for -> 3 ways
// 1. traditional for
// syntax for(initialization;condition;post iteration statement)
// initialization => optional => initialize value for counter => counts number of iter
// condition => if true, loop will execute. once it is false, loop stops.
// PIS => post iteration statement => after iteration what should happen

for (let counter = 0; counter < 10; counter++) {
    console.log(counter ** 2);
}

const sentence = "this";

for (let index = 0; index < sentence.length; index++) {
    const char = sentence[index];
    console.log(char);
}

const numbers = [1, 2, 3, 4, 5, 6];

for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    console.log(element);
}

const people = [
    { name: "heramb", surname: "joshi" },
    { name: "aniruddha", surname: "gohad" },
    { name: "shreya", surname: "khedkar" },
]

for (let index = 0; index < people.length; index++) {
    // { name: "heramb", surname: "joshi" }
    const element = people[index];
    // const name = element.name; // heramb
    // const surname = element.surname; // joshi
    const { name, surname } = element;
    console.log(`${name} ${surname}`);
}

// 2. for in loop
// syntax for(let index in string/arr/obj)
const word = "abcd";

// index => each index of the string 
for(let index in word) {
    const char = word[index];
    console.log(char);
}

// const people = [
//     { name: "heramb", surname: "joshi" },
//     { name: "aniruddha", surname: "gohad" },
//     { name: "shreya", surname: "khedkar" },
// ]

for(let index in people) {
    const person = people[index];
    const { name, surname } = person;
    console.log(`${name} ${surname}`);
}

// 3. for of loop

// const word = "abcd";
for(let char of word) {
    console.log(char);
}

// const people = [
//     { name: "heramb", surname: "joshi" },
//     { name: "aniruddha", surname: "gohad" },
//     { name: "shreya", surname: "khedkar" },
// ]

for(let person of people) {
    const {name, surname} = person;
    console.log(`${name} ${surname}`);
}


for(let { name, surname } of people) {
    console.log(`${name} ${surname}`);
}

// while
// till condition is true, loop continues 
// while(condition)


// const numbers = [1, 2, 3, 4, 5, 6];
let index = 0;

while(index < numbers.length) {
    const elem = numbers[index];
    console.log(elem);
    index += 1;
}

// [15, 16, 17, 18, 19, 20];
// LOOP NUMBER    (index) FOR IN  || FOR OF (elements)
// 1                       0          15
// 2                       1          16
// 6                       5          20 




const computers = [
    { price: 5000, brand: "abcd" },
    { price: 900, brand: "pqrs" },
    { price: 5080, brand: "lmno" },
    { price: 5900, brand: "kjd" },
    { price: 500, brand: "asdasd" },
    { price: 5000, brand: "abcd" },
    { price: 5000, brand: "abcd" },
    { price: 900, brand: "pqrs" },
    { price: 5080, brand: "lmno" },
    { price: 5900, brand: "kjd" },
    { price: 500, brand: "asdasd" },
    { price: 5000, brand: "abcd" },
];

const prices = {};

for (let {price, brand} of computers) {
    // brand => abcd
    if(!prices[brand]) {
        // attach into prices key:value brand:price
        prices[brand] = price;
    } else {
        prices[brand] += price
    }
}

console.log(prices);