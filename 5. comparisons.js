// ashe operators jyancha output always boolean ahe
// ==, !=, >, >=, <, <=

// ===, !==

// DONT USE
5 == 5; // T
5 == "a"; // F
5 == "e"; // F
5 == "5"; // T -> compares only values not datatype
5 == true; // F
5 == []; // F
"[]" == []; // F 
5 == false; // F
0 == false; // T
null == 0; // F
null == false; // false

// DONT USE
5 != 5; // F
5 != "a"; // T
5 != "e"; // T
5 != "5"; // F -> compares only values not datatype
5 != true; // T

5 < 5; // F
5 < 6; // T
4 < 5; // T
5 < 4; // F
5 < "a"; // F
"a" < 5; // F
"a" < "b"; // T 
5 < "e"; // F
5 < "5"; // F -> compares only values not datatype
5 < true; // F

5 <= 5; // T
5 <= 6; // T
4 <= 5; // T
5 <= 4; // F
5 <= "a"; // F
"a" <= 5; // F
"a" <= "b"; // T 
5 <= "e"; // F
5 <= "5"; // T -> compares only values not datatype
5 <= true; // F

5 > 5; // F
5 > 6; // F
4 > 5; // F
5 > 4; // T
5 > "a"; // F
"a" > 5; // F
"a" > "b"; // F 
5 > "e"; // F
5 > "5"; // F -> compares only values not datatype
5 > true; // F

5 >= 5; // T
5 >= 6; // F
4 >= 5; // F
5 >= 4; // T
5 >= "a"; // F
"a" >= 5; // F
"a" >= "b"; // F 
5 >= "e"; // F
5 >= "5"; // T -> compares only values not datatype
5 >= true; // F

// ===, !== -> ALWAYS USE === & !==
5 == "5"; // T -> compares only values not datatype
5 === "5"; // F -> compares both, value and datatype

5 !== "5"; // T


"1" == 1; // T
"a" == "a"; // T
10 === 10; // T
10 === "10"; // F
true === []; // F

// COMMON PITFALLS

// *************************************************************
//
// ARRAYS, OBJECT & FUNCTIONS 
// are compared, stored, copied, passed as REFERENCES not VALUES
// 
// *************************************************************

[] == []; // F
[] === []; // F
{a: 1} == {a: 1}; // F
{a: 1} === {a: 1}; // F

// directly pushed to stack
const number = 1;

// value is pushed to heap
// constant name is pushed to stack
// heap cha reference name madhe jato
let numbers = [1, 2, 3];
const numerics = [1, 2, 3]
const anotherNumbers = numbers;
anotherNumbers === numbers; // T
numbers = [4, 5, 6]
anotherNumbers === numbers; // F